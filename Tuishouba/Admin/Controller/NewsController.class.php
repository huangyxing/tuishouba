<?php
namespace Admin\Controller;
use Think\Controller;
class NewsController extends Controller{
	public function index(){
		$sort = M("Sort");
		$s = $sort->select();
		$this->assign("sort",$s);
		$title =isset($_POST["title"])?$_POST["title"]:"";
		$sortt =isset($_POST['sort'])? $_POST["sort"]:0;
		$prov = isset($_POST["province"])?$_POST["province"]:-1;
		$city = isset($_POST["city"])?$_POST["city"]:-1;
		if (empty($title)){
			$where = "";
		}else{
			$where = "title like '%".$title."%' ";
		}
		
		if($sortt != 0){
			if($where !=''){
				$where .= " and ";
			}
			$where .= "sort_id = $sortt ";
		}
		
		if($city == -1){
			
			if($prov != -1){
				if($where !=''){
					$where .= " and ";
				}
				$where .= "prov_id = $prov";
			}
			
			$city_str="";
		}else{
			if($where !=''){
				$where .= " and ";
			}
			$where .= "city_id = $city";
			
			$city_str = "";
			$city_arr =M("Place")->where("parentId = $prov")->select();
			for($i=0;$i<count($city_arr);$i++){
				if($city_arr[$i]['code'] == $city){
					$city_str .='<option value="'.$city_arr[$i]['code'].'" selected="selected" >'.$city_arr[$i]['name'].'</option>';
				}else{
					$city_str .='<option value="'.$city_arr[$i]['code'].'">'.$city_arr[$i]['name'].'</option>';
				}
			}
		}
		$new =M("News");
		$count=$new->where($where)->count();
		
		
		$page = isset($_GET['page'])?$_GET["page"]:1;
		$pagecount = 10;
		$pages = $count/$pagecount;
		$next = $page-1;
		$prev = $page+1;
		//检查总页数是否未整数
		if(!is_int($pages)){
			$pages = (int)$pages+1;
		}
		//设置分页格式
		if ($page == 1){
			$show = "<a>上一页</a>";
		}else{
			$show = "<a href='/Admin/News/index?page=$next'>上一页</a>         ";
		}
		
		for($i=1;$i<=$pages;$i++){
			if($page == $i){
				$show .= "$i  |";
			}else{
				$show .= "<a href='/Admin/News/index?page=$i'>$i</a>  |";
			}
		}
		
		if ($page == $pages){
			$show .= "最后一页了";
		}else{
			$show .= "   <a href='/Admin/News/index?page=$prev'>下一页</a>  ";
		}
				
		$pagenum = ($page-1)*$pagecount;
		//$news = $new->where($where)->limit($pagenum,$pagecount)->select();
			if($where !=''){
				$l .= " where ".$where;
			}
		$sql = "select n.*,s.name as sort_name from news as n left join sort as s on n.sort_id = s.id ".$l." order by id desc limit $pagenum,$pagecount";
		$news =$new->query($sql);
		$this->assign("page",$show);
		$this->assign("news",$news);
		$data =D("Place")->where("level = 1")->select();
		$this->assign( 'data', $data );
		$this->assign( 'title', $title );
		$this->assign( 'sortt', $sortt );
		$this->assign( 'prov', $prov );
		$this->assign( 'sortt', $sortt );
		$this->assign( 'city', $city_str );
		$this->display();
	}
	public function add(){
		$sort = M("Sort")->select();
		$this->assign( 'sort_id', $sort );
		$data =D("Place")->where("level = 1")->select();
		$this->assign( 'data', $data );
		$this->display ("add");
		}
	public function showcity() {
		$code=$_REQUEST['code'];
		$datacity =D("Place")->where("parentId=$code")->select();
		echo json_encode ( $datacity );
		}
		
	public function addOK(){
		
		$p = $_POST["province"];
		$c = $_POST["city"];
		$pname=M("Place")->where("code = $p")->find()["name"];
		$cname=M("Place")->where("code = $c")->find()["name"];
		
		$news = M("News");
		$u = $_POST["url"];
		$str="http://";
		if(strpos($u,$str) === false){     //使用绝对等于 
 		   //不包含 
			$url = "http://".$u;
		}else{
			$url = $u;
		}
		if( substr($url , 0 , 24) == "http://mp.weixin.qq.com/"){
			if(substr($url, -15) != "#wechat_redirec" ){
				$url .= "#wechat_redirect";
			}
		}
		
		$news->title = $_POST["title"];
		$news->profile = $_POST["content"];
		$news->city_id = $_POST["city"];
		$news->prov_id = $_POST["province"];
		$news->url = $url;
		$news->sort_id = $_POST["sort"];
		$news->address_str = $pname.$cname;
		
		
		if(!empty($_FILES['image']['name'])){
			$upload= new \Think\Upload();
			$upload->autoSub=true; //开启子目录保存文件
			$upload->maxSize = 2097152;//最大为2m
			$upload->subName=array('date','Y-m-d');//以时间格式生成文件夹
			$upload->exts=array('jpg','jpeg','gif','png','bmp');//可上传的类型
			$upload->savePath='News/';
			$info=$upload->upload();
			//dump($info);
		
			if(!$info){
				$this->error($upload->getError(),'add',3);
			}
		
			$news->thumb_image=$info['image']['savepath'].$info['image']['savename'];
		}
		if($news->add()){
			$this->success("添加成功",index,2);
		}else{
			$this->error("添加失败",add,2);
		}
		
	}
	public function delete(){
		$id = $_GET["id"];
		if(M("News")->where("id = $id")->delete()){
			$this->success("删除成功",index,2);
		}else{
			$this->error("删除失败",index,2);
		}
	}
	
	public function edit(){
		$data =D("Place")->where("level = 1")->select();
		$this->assign('data', $data );
		
		$id = $_GET["id"];
		$news = M("News")->where("id = $id")->find();
		$par = $news['prov_id'];
		$city =D("Place")->where("parentId = $par")->select();
		$this->assign('city', $city );
// 		dump($news);exit;
		$this->assign("news",$news);
		$this->display();
	}
	
	public function editOK(){
		$id = $_GET["id"];
		$news = M("News");
		$news->title = $_POST["title"];
		$news->profile = $_POST["profile"];
		$news->city = $_POST["city"];
		$news->prov = $_POST["province"];
		$url = $_POST["url"];
		if( substr($url , 0 , 24) == "http://mp.weixin.qq.com/"){
			if(substr($url, -15) != "#wechat_redirec" ){
				$url .= "#wechat_redirec";
			}
		}
		$news->url = $url;
		if(!empty($_FILES['image']['name'])){
			$upload= new \Think\Upload();
			$upload->autoSub=true; //开启子目录保存文件
			$upload->maxSize = 2097152;//最大为2m
			$upload->subName=array('date','Y-m-d');//以时间格式生成文件夹
			$upload->exts=array('jpg','jpeg','gif','png','bmp');//可上传的类型
			$upload->savePath='News/';
			$info=$upload->upload();
			//dump($info);
		
			if(!$info){
				$this->error($upload->getError(),'add',3);
			}
		
			$news->thumb_image=$info['image']['savepath'].$info['image']['savename'];
		}
		if($news->where("id = $id")->save()){
			$this->success("修改成功",index,2);
		}else{
			$this->error("修改失败",add,2);
		}	
	}
	
		
	
	
}