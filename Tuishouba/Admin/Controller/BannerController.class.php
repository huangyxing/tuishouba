<?php
namespace Admin\Controller;

use Think\Controller;
class BannerController extends Controller{
	public function index(){
		
		$banner = M("Banner");
	//正在显示的广告
		$count=$banner->where("display = 'Y'")->count();
		$page = isset($_GET['page'])?$_GET["page"]:1;
		$pagecount = 10;
		$pages = $count/$pagecount;
		$next = $page-1;
		$prev = $page+1;
		//检查总页数是否未整数
		if(!is_int($pages)){
			$pages = (int)$pages+1;
		}
		//设置分页格式
		if ($page == 1){
			$show = "<a>上一页</a>";
		}else{
			$show = "<a href='/Admin/Banner/index?page=$next'>上一页</a>         ";
		}
		
		for($i=1;$i<=$pages;$i++){
			if($page == $i){
				$show .= "$i  |";
			}else{
				$show .= "<a href='/Admin/Banner/index?page=$i'>$i</a>  |";
			}
		}
		
		if ($page == $pages){
			$show .= "最后一页了";
		}else{
			$show .= "   <a href='/Admin/Banner/index?page=$prev'>下一页</a>  ";
		}
		
		$pagenum = ($page-1)*$pagecount;
		$data=$banner->where("display = 'Y'")->order("lever")->limit($pagenum,$pagecount)->select();
		
		for($i=0;$i<count($data);$i++){
			$data[$i]['create_time']=date('Y-m-d H:i',$data[$i]['create_time']);
		}
		$show .="&nbsp&nbsp<span>一共".$count."条数据</span>";
		//分配变量
		$this->assign("page",$show);
		$this->assign("banner",$data);
	//正在显示的广告
		$countt=$banner->where("display = 'N'")->count();
		$paget = isset($_GET['paget'])?$_GET["paget"]:1;
		$pagecount = 10;
		$pagest = $countt/$pagecount;
		$nextt = $paget-1;
		$prevt = $paget+1;
		//检查总页数是否未整数
		if(!is_int($pagest)){
			$pages = (int)$pagest+1;
		}
		//设置分页格式
		if ($paget == 1){
			$showt = "<a>上一页</a>";
		}else{
			$showt = "<a href='/Admin/Banner/index?paget=$nextt'>上一页</a>         ";
		}
		
		for($i=1;$i<=$pagest;$i++){
			if($paget == $i){
				$showt .= "$i  |";
			}else{
				$showt .= "<a href='/Admin/Banner/index?paget=$i'>$i</a>  |";
			}
		}
		
		if ($paget == $pages){
			$showt .= "最后一页了";
		}else{
			$showt .= "   <a href='/Admin/Banner/index?paget=$prevt'>下一页</a>  ";
		}
		
		$pagenum = ($page-1)*$pagecount;
		$datat=$banner->where("display='N'")->limit($pagenum,$pagecount)->select();
		
		for($i=0;$i<count($datat);$i++){
			$datat[$i]['create_time']=date('Y-m-d H:i',$datat[$i]['create_time']);
		}
		$showt .="&nbsp&nbsp<span>一共".$countt."条数据</span>";
		//分配变量
		$this->assign("paget",$showt);
	
		$this->assign("bannert",$datat);
// 		dump($data);
// 		dump($datat);exit;
		$this->display();
	}
	
	public function showBanner(){
		
		$banner = M("Banner");
		$l = $banner->field("max(lever)")->find()["max(lever)"];
		$banner->id = $_GET["id"];
		$banner->display = 'Y';
		
		$banner->lever = $l+1;
		
		if ($banner->save()){
			
			$this->success("显示广告成功",index,2);
		}else{
			
			$this->success("显示广告失败",index,2);
		}
	}
	public function cancelBanner(){
		$banner = M("Banner");
		$id = $_GET["id"];
		$l =$banner->where("id = $id")->find()["lever"];
		$banner->display = "N";
		$banner->lever = 0;
		if($banner->save()){
			
			$sql = "update banner set  lever = lever -1 where lever > $l";
			if($banner->execute($sql)){
				$this->success("取消成功",index,2);exit;
			}
			
		}
		
		$this->error("取消失败",index,2);
	
	}
	public function deleteBanner(){
		$id = $_GET["id"];
		if(M("Banner")->where("id = $id")->delete()){
			$this->success("删除成功",index,2);
		}else{
			$this->error("删除失败",index,2);
		}
	}
	
	public function add(){
		$this->display();
	}
	
	public function addOK(){
		$banner = M("Banner");
		$banner->title = $_POST["title"];
		$banner->content = $_POST["content"];
		$banner->create_time = time();
		
		$u = $_POST["url"];
		$str="http://";
		if(strpos($u,$str) === false){     //使用绝对等于
			//不包含
			$url = "http://".$u;
		}else{
			$url = $u;
		}
		
		if( substr($url , 0 , 24) == "http://mp.weixin.qq.com/"){
			if(substr($url, -15) != "#wechat_redirec" ){
				$url .= "#wechat_redirec";
			}
		}
		
		$banner->url = $url;
		//图片上传
		if(!empty($_FILES['image']['name'])){
			$upload= new \Think\Upload();
			$upload->autoSub=true; //开启子目录保存文件
			$upload->maxSize = 2097152;//最大为2m
			$upload->subName=array('date','Y-m-d');//以时间格式生成文件夹
			$upload->exts=array('jpg','jpeg','gif','png','bmp');//可上传的类型
			$upload->savePath='Banner/';
			$info=$upload->upload();
			//dump($info);
		
			if(!$info){
				$this->error($upload->getError(),'add',3);
			}
		
			$banner->image=$info['image']['savepath'].$info['image']['savename'];
		}
		
		if($banner->add()){
			$this->success("添加成功",index,2);
		}else{
			$this->error("添加失败",add,2);
		}
	}
	
	public function edit(){
		$id=$_GET["id"];
		$banner = M("Banner")->where("id = $id")->find();
		$this->assign("banner",$banner);
		$this->display();
	}
	
	public function editOK(){
		$banner = M("Banner");
		$id = $_POST["id"];
		$banner->title = $_POST["title"];
		
		$banner->create_time = time();
		
		$u = $_POST["url"];
		$str="http://";
		if(strpos($u,$str) === false){     //使用绝对等于
			//不包含
			$url = "http://".$u;
		}else{
			$url = $u;
		}
		
		if( substr($url , 0 , 24) == "http://mp.weixin.qq.com/"){
			if(substr($url, -15) != "#wechat_redirec" ){
				$url .= "#wechat_redirec";
			}
		}
		
		$banner->url = $url;
		//图片上传
		if(!empty($_FILES['image']['name'])){
			$upload= new \Think\Upload();
			$upload->autoSub=true; //开启子目录保存文件
			$upload->maxSize = 2097152;//最大为2m
			$upload->subName=array('date','Y-m-d');//以时间格式生成文件夹
			$upload->exts=array('jpg','jpeg','gif','png','bmp');//可上传的类型
			$upload->savePath='Banner/';
			$info=$upload->upload();
			//dump($info);
		
			if(!$info){
				$this->error($upload->getError(),'add',3);
			}
		
			$banner->image=$info['image']['savepath'].$info['image']['savename'];
		}
		
		if($banner->where("id=$id")->save()){
			$this->success("修改成功",index,2);
		}else{
			$this->error("修改失败","/Admin/Banner/edit?id=$id",2);
		}
	}
	
	
}