<?php
namespace Admin\Controller;
use Think\Controller;
class IndexController extends Controller {
	
	public function index(){
		if(!isset($_COOKIE["manager_id"])){
			$this->login();exit;
		}else{
			$_SESSION["partner"]["id"]=$_COOKIE["manager_id"];
		}
		
		if(isset($_SESSION['manager']['name'])){
			$name = $_SESSION['manager']['name'];
		}else{
			$name = "未登录";
		}
		$this->assign("name",$name);
		$this->display ();
    }
    public function login(){
    	$Verify = new \Think\Verify();
    	$this->assign('Verify',$Verify);
    	$this->display("login");
    }
public function Verify(){
			
		$verify=new \Think\Verify();
		$verify->length=4;
		$verify->codeSet='0123456789';
		$verify->fontSize=25;
		$verify->useCurve=false;
		$verify->useNoise=false;
	
		$verify->entry();
			
	}
	public function checklogin(){
	
		$code=I('post.verify');
	
		$name=I('post.username');
		$password=md5(I('post.password'));
	
		$verify =new \Think\Verify();
		if(!$verify->check($code)){
			$this->error('验证码错误','login',3);
		}
		if (empty($code) || empty($name)){
			$this->error('用户名和密码不能为空','login',3);
		}
		 
		$user=M('Manager');
		$flag=$user->where("username='$name' and password='$password' ")->field("id")->find();
		 
		if(isset($flag)){
			$_SESSION['manager']['name']=$name;
			$_SESSION['manager']['id']=$flag["id"];
			cookie("manager_id",$flag["id"],3600);
			$this->success('登录成功','/Admin/Index/index',3);
			 
		}else{
			$this->error('用户名或密码错误，请重新的登录','/Admin/Index/login',3);
		}
	}
	
	public function logout(){
	
		cookie('manager_id',null);
		$_SESSION["mananger"] = null;
		$this->success("退出成功","/Admin/Index/login",2);
	}
	
	public function changePassword(){
		$this->display("manage");
	}
	
	public function changePasswordOK(){
		$manage_id = $_SESSION['manager']['id'];
		$oldpw = $_POST["oldpw"];
		$pw = $_POST["password"];
		$repw = $_POST["repassword"];
		$ma = M("Manager")->where("id =$manage_id")->find();
		
		if (md5($oldpw) !=$ma["password"]){
			$this->error('原密码不正确','/Admin/Index/changePassword',3);
		}
		if($oldpw == $pw){
			$this->error('新密码与原密码相同','/Admin/Index/changePassword',3);
		}
		if($pw == $repw){
			$manage = M("Manager");
			$manage->password = md5($pw);
			if($manage->where("id = $manage_id")->save()){
				$this->success('密码修改成功','/Admin/Index/index',3);
			}
		}else{
			$this->error('两次密码不一致，请重新输入','/Admin/Index/changePassword',3);
		}
	}
	
}