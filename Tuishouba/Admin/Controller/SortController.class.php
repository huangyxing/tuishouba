<?php
namespace Admin\Controller;
use Think\Controller;
class SortController extends Controller{
	public function index(){
		$sort = M("Sort")->select();
		$this->assign("sort",$sort);
		$this->display();
	}
	public function add(){
		$this->display();
	}
	public function addOK(){
		$sort = M("Sort");
		$sort->name = $_POST["name"];
		if ($sort->add()){
			$this->success("添加成功",index,1);
		}else{
			$this->error("添加失败",add,1);
		}
		
	}
	
	public function edit(){
		$id = $_GET["id"];
		$sort = M("Sort")->where("id = $id")->find();
		$this->assign("sort",$sort);
		$this->display();
	}
	public function editOK(){
		$sort = M("Sort");
		$id = $_REQUEST['id'];
		$sort->id = $id;
		$sort->name = $_REQUEST["name"];
		if ($sort->save()){
			$this->success("修改成功",index,1);
			}else{
				$this->error("修改失败","edit?id=$id" ,1);
			}
	}
	
	public function delete(){
		dump($_REQUEST);
	}
}